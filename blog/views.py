from django.shortcuts import get_object_or_404, render
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse
from django.views import generic

from blog.models import Post, Comment

# Create your views here.
class IndexView(generic.ListView):
  template_name = 'blog/index.html'
  model = Post

class DetailView(generic.DetailView):
  model = Post
  template_name = 'blog/detail.html'

def comment(request, post_id):
  post = get_object_or_404(Post, pk=post_id)
  post.comment_set.create(name=request.POST['name'], text=request.POST['text'])
  return HttpResponseRedirect(reverse('detail', args=(post.id,)))
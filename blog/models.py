from django.db import models

# Create your models here.
class Post(models.Model):
  title = models.CharField(max_length=255)
  slug = models.SlugField(unique=True, max_length=255)
  description = models.CharField(max_length=255)
  content = models.TextField()
  published = models.BooleanField(default=True)
  created_at = models.DateTimeField(auto_now_add=True)

  class Meta:
    ordering = ['-created_at']

  def __unicode__(self):
    return self.title

class Comment(models.Model):
  post = models.ForeignKey(Post)
  name = models.CharField(max_length=255)
  text = models.TextField()
  created_at = models.DateTimeField(auto_now_add=True)

  class Meta:
    ordering = ['-created_at']

  def __unicode__(self):
    return self.name

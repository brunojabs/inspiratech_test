from django.contrib import admin
from blog.models import Post, Comment
# Register your models here.



class PostAdmin(admin.ModelAdmin):
  readonly_fields = ['created_at']

class CommentAdmin(admin.ModelAdmin):
  readonly_fields = ['post', 'name', 'text', 'created_at',]

admin.site.register(Post, PostAdmin)
admin.site.register(Comment, CommentAdmin)
